const Config = {
    defaultPath: '/inicio-sesion',
    basename: '/able-pro/react/default/',
    layout: 'vertical',
    subLayout: '',
    collapseMenu: false,
    layoutType: 'menu-light',
    headerBackColor: 'header-blue',
    rtlLayout: false,
    navFixedLayout: true,
    headerFixedLayout: false,
    boxLayout: false
};
export default Config;

import * as React from 'react';
const PreCotizacion = React.lazy(() => import('./Demo/Forms/PreCotizacion'));
const SignIn = React.lazy(() => import('./Demo/Authentication/SignIn2'))
//Aqui se pondran las rutas para las pantallas de inicio de sesion, dar de alta nuevo cliente, precotizacion, etc.
//todo lo que tenga que ver conlo previo al menu principal
const route = [
    { path: '/precotizacion', exact: true, name: 'Precotizacion', component: PreCotizacion },
    { path: '/inicio-sesion', exact: true, name: 'Inicio sesion', component: SignIn }
];
export default route;

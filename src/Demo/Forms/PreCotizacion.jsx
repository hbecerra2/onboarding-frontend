import * as React from 'react';
import { useState, useEffect } from 'react';
import { Row, Col, Card, Button, Container, Image, InputGroup, FormGroup, InputAdornment} from 'react-bootstrap';
import Select from 'react-select';
import NumberFormat from 'react-number-format';
import { ValidationForm, TextInput, BaseFormControl, SelectGroup, FileInput, Checkbox, Radio } from 'react-bootstrap4-form-validation';
import MaskedInput from 'react-text-mask';
import validator from 'validator';
import AnimatedModal from '../../App/components/AnimatedModal';
import logoBlanco from './../../assets/images/Kualinetb.svg'
import logoNegro from './../../assets/images/Kualinet.svg'
import './../../assets/scss/style.scss';
import Aos from "aos";
import "aos/dist/aos.css";

// objeto que sera para el select de plazo
export const plazo =[
    {value: 1, label:'6 Meses'},
    {value: 2, label: '9 Meses'},
    {value: 3, label:'12 Meses'},
    {value: 4, label:'15 Meses'},
    {value: 5, label:'18 Meses'},
    {value: 6, label:'21 Meses'},
    {value: 7, label:'24 Meses'},
]

// objeto que sera para el select de garantia
export const garantia = [
    {value: 1, label: 'Inmobiliaria'},
]

//objeto que sera para el select de actividad economica
export const selectActividad = [
    {value: 1, label:'Comercio'},
    {value: 2, label:'Servicios'},
    {value: 3, label:'Manufactura'},
    {value: 4, label:'Construcción'},
    {value: 5, label:'Minería'},
    {value: 6, label:'Transporte y almacenamiento'},
    {value: 7, label:'Agricultura, cría y explotación de animales'},
    {value: 8, label:'Aprovechamiento forestal, pesca y caza'}
]

//objeto que sera para el select de destino de credito
export const selectDestino = [
    {value: 1, label: 'Compra de materia prima'},
    {value: 2, label: 'Compra de inventario'},
    {value: 3, label: 'Contratación de personal'},
    {value: 4, label: 'Nuevos puntos de venta'},
    {value: 5, label: 'Marketing y publicidad'},
    {value: 6, label: 'Tecnologías de la información y comunicación'},
    {value: 7, label: 'Compra de maquinaria y equipo'},
    {value: 8, label: 'Instalaciones y edificaciones'},
    {value: 9, label: 'Otro destino'}
    
]

const FormsValidation = () => {
//Variables de estado
const[Express, SetExpress] = useState(true);
const[Plus, SetPlus] = useState(false);
const[Monto, setMonto]= useState(null);
const [plazoSelect, setPlazoSelect] = useState([])
const[garantiaSelect, setGarantiaSelect] = useState([]);
const[actividadE, setActividadE] = useState([]);
const[destinoC, setDestinoC] = useState([]);
const[formulario, setFormulario] = useState(true);
const[resultado, setResultado] = useState(false);

    //use effect para poder controlar el tiempo de las animaciones fade
    useEffect(()=>{
        Aos.init({duration: 1500});
    },[]);
    

    

    //handleMonto sera una funcion la cual recibe el monto puesto por el usuario
    //recibe el value del input monto, se splitea para separar el simbolo de $
    //y una vez que se cree el indice 1, tomara el valor del indice 1 el cual contendra numeros
    //dichos numeros del indice 1 son tomados y convertidos a flotante, tras esto se asignan 
    //a la variable de estado "Monto"
    const handleMontoExpress = (value) => {
       setMonto(value)
    };

    //Lo mismo que la funcion de arriba pero para perfil PLUS
    const handleMontoPlus = (value) => {
        console.log("valor del value"+value)
        setMonto(value)
    };

 //handle plazo Express y Plus recibe el plazo que selecciona el usuario y asignar el valor 
 //numerico del arreglo a la variable de estado
    const handlePlazoExpress = (elPlazo) => {
        setPlazoSelect(elPlazo)
      
    };

    const handlePlazoPlus = (elPlazo) => {
        setPlazoSelect(elPlazo)
        
    };
    
  //funcion para asignar a la variable de estado la garantia seleccionada por el usuario
    const handleGarantia = (laGarantia) => {
        setGarantiaSelect(laGarantia);
        

    };

    const handleActividadExpress =(laActividad) =>{
        setActividadE(laActividad);
    };

    const handleActividadPlus =(laActividad) =>{
        setActividadE(laActividad);

    }

    const handleDestinoExpress =(elDestino)=>{
        setDestinoC(elDestino);
    }

    const handleDestinoPlus =(elDestino)=>{
        setDestinoC(elDestino)
    }

    //funciones donde se mandara hacer el calculo, verifica que los campos ya tengan un valor
    const botonasoPlus = () =>{
        if(Monto<100000){
            alert("Tu monto es menor a $ 100,000 pesos mexicanos, KUALINET solo otorga prestamos minimos de $ 100,000 pesos mexicanos en adelante.")
            setMonto(null)
        }
        else{
            if(Monto>2000000){
                alert("Tu monto es mayor a $ 2,000,000.00 de pesos mexicanos, sobrepasa el limite de KUALINET, porfavor ingrese una cifra menor")
                setMonto(null)
            }
            else{
                if((Monto !== null && Monto !== isNaN) && (plazoSelect !== null) && (garantiaSelect !== null) && (actividadE !== null) && (destinoC !== null)){
                    
                    setFormulario(false)
                    setResultado(true)
                }
                else{
                    alert("Por favor, rellene los campos solicitados")
                }
            }
         }
    };

    const botonasoExpress = () =>{
        if(Monto<100000){
            alert("Tu monto es menor a $ 100,000 pesos mexicanos, KUALINET solo otorga prestamos minimos de $ 100,000 pesos mexicanos en adelante.")
            setMonto(null)
        }
        else{
            if(Monto>500000){
                alert("Tu monto es mayor a $ 500,000 pesos mexicanos, si necesitas una cifra mas grande selecciona KUALINET PLUS ")
                setMonto(null)
            }
            else{
                if((Monto !== null && Monto !== isNaN) && (plazoSelect !== null ) && (actividadE !== null) && (destinoC !== null)){
                    setFormulario(false)
                    setResultado(true)
                }
                else{
                    alert("Por favor, rellene los campos solicitados")
                }
            }
        }
    };

    //Funcion para volver a renderizar el formulario de la precotizacion
    const formularioReturn = () =>{
        setResultado(false)
        setFormulario(true)
        //-----VALORES DEL FORMULARIO---
        setPlazoSelect(null);
        setGarantiaSelect(null);
        setActividadE(null);
        setDestinoC(null);

    }

  
//funciones para activar las modalidades ya sea express o plus
    function activarExpress(){
        setMonto(null)
        SetExpress(true)
        SetPlus(false)
        setPlazoSelect(null)
        setGarantiaSelect(null)
        setActividadE(null)
        setDestinoC(null)
    }

    function activarPlus(){
        setMonto(null)
        SetPlus(true)
        SetExpress(false)
        setPlazoSelect(null)
        setGarantiaSelect(null)
        setActividadE(null)
        setDestinoC(null)
    }

    //Funcion para mostrar en formato de cifras monetarias el monto del cliente
    function formatNumber(numerillo){
        return new Intl.NumberFormat("ES-MX", {
            style: "currency",
            currency: "MXN"
          }).format(numerillo)
    }

 //------------------------------------------------------------------------------------
//inicio de renderizado del componente
    return (<>
    <br/>
{formulario && 
    <Col>  
  
    {/*AQUI SE ACOMODA LA IMAGEN DE KUALINET */}
            <Container data-aos="fade-down" style={{position: 'relative', display: "flex",
                justifyContent: "center",
                alignItems: "center"}}>
                                <Col xs={10} md={4}>
                                <Image src ={logoBlanco}/>
                                </Col>
            </Container>
      <br/> <br/>


      {/*Este  container es para el formulario */}                  
    <Container  style={{position: 'relative', display: "flex",
          justifyContent: "center",
          alignItems: "center"}}>  

    {/*Primero se renderiza por default la modalidad express */}
        { Express &&
        <Card data-aos="fade">
                <Card.Header>
                        <Card.Title as="h3" data-aos="fade-right">Un crédito a tu medida</Card.Title>
                </Card.Header>
                    <Card.Body>
                        <Row>
                            <Col>
                                <Card onClick={()=> activarExpress()} data-aos="flip-right"> 
                                <Card.Body>  
                                
                                    <Button style = {{ fontWeight: "bold"}} size = "sm" disabled variant ="light">
                                    Hasta $500,000 Pesos
                                    </Button>

                                    <h1 style = {{ fontWeight: "bold"}}>$Kualinet</h1>
                                    <h1  style = {{ fontWeight: "bold"}}>EXPRESS</h1>

                                    <p className="mb-2" style = {{ fontWeight: "bold", color: "#ADB5BD"}}>Sin Garantia inmobiliaria</p>
                                </Card.Body>    
                            </Card>
                            </Col>

                            <Col>
                                
                                <Card onClick={()=> activarPlus()} data-aos="flip-left" style={{backgroundColor: "#1FA2FF" }}> 
                                <Card.Body>
                                <Button style= {{fontWeight: "bold", color: "#181C20", backgroundColor: "#1C91E5"}}size = "sm" disabled>
                                    <b>Hasta $2,000,000 Pesos</b>
                                </Button>
                                    <h1 style = {{color: "#FFFFFF"}}>$Kualinet</h1>
                                    <h1 style = {{color: "#FFFFFF"}}>PLUS+</h1>

                                    <p className="mb-2 " style = {{fontWeight: "bold", color: "#105483"}}>Con Garantia inmobiliaria</p>
                                </Card.Body>
                                </Card>
                              
                            </Col>
                        </Row>

                        {/*INICIO DEL FORMULARIO PARA PRE COTIZACION */}
                        
                        

                            <div data-aos="fade-right" className="input-group mb-3">
                                <span  className="input-group-text" style={{backgroundColor: "#1C3FAA", color: "#FFFFFF", fontWeight: "bold"}}>Monto</span>
                                <Col>
                                <div className="input-group"> 
                                    <span className="input-group-text">$</span>
                                    <NumberFormat  className="form-control" value= {Monto || ""}  onInput={e => handleMontoExpress(e.target.value)}/>
                                </div>
                                    {console.log(Monto)}
                                </Col>
                            </div>
                                
                           
                            <div  className="input-group mb-3">
                                <span data-aos="fade-left" className="input-group-text" style={{backgroundColor: "#1C3FAA", color: "#FFFFFF", fontWeight: "bold"}}>Plazo</span>
                                <Col>
                                    <Select className="basic-single" classNamePrefix="select" placeholder="Seleccione un plazo" options={plazo} onChange={e => handlePlazoExpress(e)}/>
                                    {console.log(plazoSelect)}
                                </Col>
                            </div>
                            
                            <div  className="input-group mb-3">
                                <span data-aos="fade-down" className="input-group-text" style={{backgroundColor: "#1C3FAA", color: "#FFFFFF", fontWeight: "bold"}}>Garantia</span>
                                <Col>
                                    <h4>No disponible en Express</h4>
                                </Col>
                            </div>
                    
                            <br/>
                           
                           
                            <Select className="basic-single" classNamePrefix="select"  placeholder="¿Que actividad económica realizas?" options={selectActividad} onChange={e => handleActividadExpress(e)}/>
                            {console.log(actividadE)}
                            <br/>
                            
                            <Select className="basic-single" classNamePrefix="select"  placeholder="¿Cual es el destino de tu crédito?" options={selectDestino} onChange={e => handleDestinoExpress(e)}/>
                            {console.log(destinoC)}
                            <br/> <br/> 

                            {/*BOTON PARA HACER EL CALCULO */}
                          
                             <Container  style={{position: 'relative', display: "flex",
                                justifyContent: "center",
                                alignItems: "center"}}>
                                
                                <Button onClick={()=>botonasoExpress()}  variant="primary" size="lg" style={{backgroundColor: "#1C3FAA"}}>
                                    <h3 style={{ color: "#FFFFFF", fontWeight: "bold"}}>Calcular</h3>
                                </Button>

                             </Container>
                   
                         
                        </Card.Body>
                    </Card>
}

{/*EN CASO DE QUE SE SELECCIONE EL CREDITO DE PLUS, SE RENDERIZARA ESTA TARJETA */}

{Plus &&
      <Card data-aos="fade" style={{backgroundColor: "#1FA2FF"}}>
      <Card.Header>
              <Card.Title as="h3" data-aos="fade-right" style={{color: "#FFFFFF"}}>Un crédito a tu medida</Card.Title>
      </Card.Header>
          <Card.Body>
              <Row>
                  <Col>
                      <Card onClick={()=> activarExpress()} data-aos="flip-right"> 
                      <Card.Body>  
                      
                          <Button style = {{ fontWeight: "bold"}} size = "sm" disabled variant ="light">
                          Hasta $500,000 Pesos
                          </Button>

                          <h1 style = {{ fontWeight: "bold"}}>$Kualinet</h1>
                          <h1  style = {{ fontWeight: "bold"}}>EXPRESS</h1>

                          <p className="mb-2" style = {{ fontWeight: "bold", color: "#ADB5BD"}}>Sin Garantia inmobiliaria</p>
                      </Card.Body>    
                  </Card>
                  </Col>

                  <Col>
                      
                      <Card onClick={()=> activarPlus()} data-aos="flip-left" style={{backgroundColor: "#1FA2FF" }}> 
                      <Card.Body>
                      <Button style= {{fontWeight: "bold", color: "#181C20", backgroundColor: "#1C91E5"}}size = "sm" disabled>
                          <b>Hasta $2,000,000 Pesos</b>
                      </Button>
                          <h1 style = {{color: "#FFFFFF"}}>$Kualinet</h1>
                          <h1 style = {{color: "#FFFFFF"}}>PLUS+</h1>

                          <p className="mb-2 " style = {{fontWeight: "bold", color: "#105483"}}>Con Garantia inmobiliaria</p>
                      </Card.Body>
                      </Card>
                    
                  </Col>
              </Row>

              {/*INICIO DEL FORMULARIO PARA PRE COTIZACION */}
              
                  <div data-aos="fade-right" className="input-group mb-3">
                      <span  className="input-group-text" style={{backgroundColor: "#1C3FAA", color: "#FFFFFF", fontWeight: "bold"}}>Monto</span>
                      <Col >
                      <div className="input-group"> 
                      <span className="input-group-text">$</span>
                      <NumberFormat style ={{backgroundColor: "#1FA2FF", color: "#FFFFFF"}}  className="form-control" value= {Monto || ""}  onInput={e => handleMontoPlus(e.target.value)}/>
                        </div> 
                                    {console.log(Monto)}
                      </Col>
                  </div>
                      
                 
                  <div  className="input-group mb-3">
                      <span data-aos="fade-left" className="input-group-text" style={{backgroundColor: "#1C3FAA", color: "#FFFFFF", fontWeight: "bold"}}>Plazo</span>
                      <Col>
                          <Select className="basic-single" classNamePrefix="select" placeholder="Seleccione un plazo" options={plazo} onChange={e => handlePlazoPlus(e)}/>
                          {console.log(plazoSelect)}
                      </Col>
                  </div>
                      
                 
        
                  <div  className="input-group mb-3">
                      <span data-aos="fade-down" className="input-group-text" style={{backgroundColor: "#1C3FAA", color: "#FFFFFF", fontWeight: "bold"}}>Garantia</span>
                      <Col>
                          <Select className="basic-single" classNamePrefix="select" placeholder="Seleccione una garantia" options={garantia} onChange={e=> handleGarantia(e) }/>
                          {console.log(garantiaSelect)}
                      </Col>
                  </div>
                  <br/>
                 
                 
                  <Select className="basic-single" classNamePrefix="select"  placeholder="¿Que actividad económica realizas?" options={selectActividad} onChange = {e=>handleActividadPlus(e)}/>
                  <br/>
                  
                  <Select className="basic-single" classNamePrefix="select"  placeholder="¿Cual es el destino de tu crédito?" options={selectDestino} onChange = {e=>handleDestinoPlus(e)}/>
                  <br/> <br/> 
                  {/*BOTON PARA HACER EL CALCULO */}
                
                   <Container  style={{position: 'relative', display: "flex",
                      justifyContent: "center",
                      alignItems: "center"}}>
       
                      <Button onClick={() => botonasoPlus()}  variant="primary" size="lg" style={{backgroundColor: "#1C3FAA"}}>
                          <h3 style={{ color: "#FFFFFF", fontWeight: "bold"}}>Calcular</h3>
                      </Button>
                   </Container>
         
               
              </Card.Body>
          </Card>

}
        
        </Container>


    </Col>
}

{/*renderizado condicional donde se mostrara la precotizacion ya hecha y el boton para crear el usuario */}
{ resultado &&

<Container  style={{
          justifyContent: "center",
          alignItems: "center"}}>  
    <Card data-aos="fade-up">
        <Card.Header>
            <Row>
            <Col>
            <a href ="#"onClick={formularioReturn}>Regresar al formulario</a>
            <br/>
            <br/>
            <br/>
            <Card.Title data-aos="fade-right" as="h2" style = {{color: "#1F40AA"}}>DETALLE</Card.Title>
            </Col>
            <Col >
                <Image data-aos="fade-left" src ={logoNegro}/>
            </Col>
            </Row>
                </Card.Header>
                    <Card.Body>
                        
            
                            <Row className="text-right">
                                <Col>
                            <h5  className="text-muted">Precotizacion</h5>
                                </Col>
                            </Row>
                            <hr/>
                            
                               
                                <Row > 
                                <Col className="text-left">
                                <Card.Text>Detalle de tu Crédito</Card.Text>
                                </Col>
                                <Col className="text-right">
                                <Card.Text>SUBTOTAL:</Card.Text>
                                </Col>
                                </Row>
               
                                <hr/>
                                <Row > 
                                <Col className="text-left">
                                <Card.Text>Monto Solicitado:</Card.Text>
                                </Col>
                                <Col className="text-right">
                                <Card.Text>{formatNumber(Monto)}</Card.Text>
                                </Col>
                                </Row>
                                <hr/>
                                <Row > 
                                <Col className="text-left">
                                <Card.Text>Monto a Acreditar</Card.Text>
                                </Col>
                                <Col className="text-right">
                                <Card.Text>-----</Card.Text>
                                </Col>
                                </Row>
                                <hr/>
                                <Row > 
                                <Col className="text-left">
                                <Card.Text>Cuota Promedio</Card.Text>
                                </Col>
                                <Col className="text-right">
                                <Card.Text>---------</Card.Text>
                                </Col>
                                </Row>
                                <hr/>
                                <Row > 
                                <Col className="text-left">
                                <Card.Text>Plazo</Card.Text>
                                </Col>
                                <Col className="text-right">
                                <Card.Text>{plazoSelect.label}</Card.Text>
                                </Col>
                                </Row>
                                <hr/>
                                <Row > 
                                <Col className="text-left">
                                <Card.Text>Tasa Nominal Anual(TNA)</Card.Text>
                                </Col>
                                <Col className="text-right">
                                <Card.Text>-----------</Card.Text>
                                </Col>
                                </Row>
                                <hr/>
                                <Row > 
                                <Col className="text-left">
                                <Card.Text>Comision uso de plataforma</Card.Text>
                                </Col>
                                <Col className="text-right">
                                <Card.Text>-----------</Card.Text>
                                </Col>
                                </Row>
                                <hr/>
                                <Row > 
                                <Col className="text-left">
                                <Card.Text>Costo Anual Total(CAT) s/IVA</Card.Text>
                                </Col>
                                <Col className="text-right">
                                <Card.Text>-----------</Card.Text>
                                </Col>
                                </Row>
                                <hr/>
                                <Row > 
                                <Col className="text-left">
                                <Card.Text>Costo Anual Total(CAT) C/IVA</Card.Text>
                                </Col>
                                <Col className="text-right">
                                <Card.Text>-----------</Card.Text>
                                </Col>
                                </Row>
                                <hr/>
                                           
                         <br/>
                    <Container  style={{position: 'relative', display: "flex",
                      justifyContent: "center",
                      alignItems: "center"}}>
       
                        <Button   size="lg" style={{border: "none", backgroundColor: "#00A9D7"}}>
                            <h3 style={{ color: "#FFFFFF", fontWeight: "bold"}}>¡Crear mi usuario ya!</h3>
                        </Button>
                   </Container>
                        
             </Card.Body>
    </Card>
</Container>
}
        </>);
};
export default FormsValidation;
//{formatNumber(Monto)}
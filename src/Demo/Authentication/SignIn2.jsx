import * as React from 'react';
import { NavLink } from 'react-router-dom';
import { Fade, Container, Form, Image, Row } from 'react-bootstrap';
import { useState, useEffect } from 'react';
import './../../assets/scss/style.scss';
import logoBlanco from './../../assets/images/Kualinetb.svg'
import logoUsuario from './../../assets/images/usuario.svg'
import logoNegro from './../../assets/images/Kualinet.svg'

const SignIn2 = () => {
const[fade, setFade] = useState(false); //Las variables fade solo son para el efecto como de desbanecimiento
const[fade2, setFade2] = useState(false);

//Se utiliza aqui un useEffect para poder utilizar el efecto de fade en los elementos del componente
useEffect(() =>{
    async function Esperar(){
      new Promise(function(){
    setTimeout(function() {setFade(true);}, 500);
    })

  }
  async function Esperar2() {
    new Promise(function(){
  setTimeout(function() {setFade2(true);}, 1000);
  })

}
  Esperar(); Esperar2()},[])

    return (<>
         
            <div className="auth-wrapper align-items-stretch aut-bg-img">
                {/*Aqui es donde esta el fondo azul y las imagenes */}
                <div className="flex-grow-1">
                
                    <div className="h-100 d-md-flex align-items-center auth-side-img">
                        
                        <Row>
                        <Fade in ={fade}>
                        <Image className = "KualinetLogo"  src = {logoBlanco}/>
                        </Fade>
                        </Row>
                        <Row>
                        <Fade in ={fade2}>
                        <Image className = "usuarioLogo" src = {logoUsuario}/>
                        </Fade>
                        </Row>
                        
                        <div className="col-sm-10 auth-content w-auto">
                        
                        <br></br><br></br><br></br><br></br><br></br><br></br><br></br><br></br><br></br><br></br><br></br>
                        <br></br><br></br><br></br><br></br><br></br><br></br><br></br><br></br><br></br><br></br><br></br>
                        <Fade in ={fade2}>
                            <h3 className="text-white font-weight-normal">
                                Unos pocos clics más para
                                <br />
                                acceder a un Plus+
                            </h3>
                        </Fade>
                            
                        </div>
                        
                    </div>

                    {/*Aqui inicia el recuadro blanco para el login */}
                    <div className="auth-side-form">
                        <div className=" auth-content">
                        <Fade in ={fade}>
                            <h1 className="mb-4 f-w-400">Bienvenido</h1>
                        </Fade>
                        <Fade in ={fade2}>
                            <Image className="KualinetNegro" src = {logoNegro}></Image>
                        </Fade>
                        <Fade in ={fade2}>
                            <h3 className="mb-4 f-w-400">Inicio Sesión</h3>
                        </Fade>
                            <div className="form-group fill">
                            <Fade in ={fade2}> 
                                <input type="email" className="form-control" placeholder="Email"/>
                            </Fade>
                            </div>
                            <div className="form-group fill mb-4">
                            <Fade in ={fade2}>
                                <input type="password" className="form-control" placeholder="Contraseña"/>
                            </Fade>
                            </div>
                        
                           
                            <Fade in ={fade2}>
                            <button className="shadow-lg btn btn-block btn-primary mb-0">Iniciar sesion</button>
                            </Fade>
                            <br></br><br></br>
                            <Fade in ={fade2}>
                            <h5 className="mb-2 text-center"> ¿Aun no tienes una cuenta?</h5>
                            </Fade>

                            <NavLink to = "/precotizacion" className = "f-w-400">
                            <Fade in ={fade2}>
                            <button  className="shadow-lg btn-lg btn btn-block btn-info">¡Crear cuenta!</button>
                            </Fade>
                            </NavLink>
                            
                            <div className="text-center">
                                <div className="saprator my-5">
                                <Fade in ={fade}>
                                    <span>O</span>
                                </Fade>
                                </div>
                                <Fade in ={fade}>
                                <p className="mb-2 text-muted">
                                    ¿Olvidaste tu contraseña?{' '}
                                    <NavLink to="/auth/reset-password-2" className="f-w-400">
                                        Recuperar
                                    </NavLink>
                                </p>
                                </Fade>
                               
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </>);
};
export default SignIn2;

import * as React from 'react';
import { lazy, Suspense } from 'react';
import { Switch, Route, useLocation, Redirect } from 'react-router-dom';
import '../../node_modules/font-awesome/scss/font-awesome.scss';
import Loader from './layout/Loader';
import ScrollToTop from './layout/ScrollToTop';
import routesOnePage from '../route';
import routes from '../routes';
import { Container, Col, Row, Card, Form, Image } from 'react-bootstrap';
import SignIn2 from '../Demo/Authentication/SignIn2';
import Config from '../config';
import PreCotizacion from '../Demo/Forms/PreCotizacion'

const AdminLayout = lazy(() => import('./layout/AdminLayout'));
const App = () => {
    const location = useLocation();
    var width = window.screen.width;
    var height = window.screen.height;
    let alturaPorAnchura = width*height;
  //   <PreCotizacion/> <SignIn2/>
  
    return (
    <>
    <Suspense fallback={<Loader />}>
            <Route path={routesOnePage.map((x) => x.path)}>
                <Switch location={location} key={location.pathname}>
                    {routesOnePage.map((route, index) => {
                        return route.component ? (<Route key={index} path={route.path} exact={route.exact}
                        render={(props) => <route.component {...props}/>}/>) : null;})}
                </Switch>
            </Route>
            <Route path={routes.map((x) => x.path)}>
                  <SignIn2/>
            </Route>
            <Route path={'/'} exact>
                <Redirect to={Config.defaultPath}/>
            </Route>
    </Suspense>
    <div className="backdrop"/>
    </> 
    );
};
export default App;
